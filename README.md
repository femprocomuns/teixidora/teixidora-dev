This repo contains everything you need to set up a docker-based development environment for the semantic wiki Teixidora. It aims to be a simplified version of the wiki running at https://teixidora.net

This work is based on the Docker Hub Mediawiki 1.27 docker image.

## Prerequisites
- Docker
- Bash
- A local clone of this repo

## Building the Docker images
Build time is around 5min on a AMD Ryzen 3 2200G including DB import (but not SMW rebuild)

1. Build the mw-composer docker image FROM mediawiki 1.27.3 image and tag it. This image installs php composer and some apt packages that will be needed later.
```
    cd mw-composer/
    docker build -t mw-composer:1.27.3 .
```
2. Remove previous containers and volumes (rename them if you want to keep them):
```
    cd docker-compose/
    docker-compose down
    docker volume rm docker-compose_teixidora-data
```
3. Run compose.sh. This script will bring up the mw and mariadb containers using the docker-compose-base.yml file, do the initial install of MW, run composer to install php dependencies and download and install extra skins and extensions. It will also make some changes to the LocalSettings.php file created by the installer.
```
    ./compose.sh
```
4. Import the MySQL dump. The mariadb container uses a persistent volume so you only need to do this once. The following commands assume that you have copied the mysql dump file and the static assets directory to the docker-compose directory.
```
    docker cp teixidora_net.sql docker-compose_database_1:/tmp/
    docker exec docker-compose_database_1 bash -c 'mysql -uroot -pmyrootpass my_wiki < /tmp/teixidora_net.sql'
    docker exec docker-compose_database_1 bash -c 'rm -f /tmp/teixidora_net.sql'
```
5. Import static assets:
```
    docker cp teixidora-files/ docker-compose_mediawiki_1:/var/www/html/
    docker exec docker-compose_mediawiki_1 bash -c 'mv teixidora-files/* images/ && rmdir teixidora-files';
```
6. Update the DB:
```
    docker exec docker-compose_mediawiki_1 bash -c 'php maintenance/update.php --quick'
```
7. Rebuild SMW data, you probably don't need to do this (it will take several hours):
```
    docker exec docker-compose_mediawiki_1 bash -c 'php extensions/SemanticMediaWiki/maintenance/rebuildData.php'
```
8. docker commit to save changes:
```
    docker commit docker-compose_mediawiki_1 teixidora_mw:0.2
    docker commit docker-compose_database_1 teixidora_db:0.2
```


