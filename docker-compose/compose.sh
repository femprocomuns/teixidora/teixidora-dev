#! /usr/bin/env bash

set -euf -o pipefail

# Remove existing containers
docker-compose rm --force

# Bring up the environment
docker-compose -f docker-compose-base.yml up -d
echo "--> docker-compose up"
echo "--> Waiting for db to be ready"
sleep 20

# Configure MW
echo "--> Initial MediaWiki configuration"
docker exec docker-compose_mediawiki_1 php maintenance/install.php --server http://localhost:8080 --scriptpath "/w" --dbserver docker-compose_database_1 --dbuser wikiuser --dbpass wikipass --pass wikipass Teixidora wikiuser

# Add the MW path to Apache
echo "--> Configuring Apache"
docker exec docker-compose_mediawiki_1 sh -c 'echo "Alias /w /var/www/html" >> /etc/apache2/apache2.conf'
docker exec docker-compose_mediawiki_1 apachectl graceful

# Install extensions and PHP dependencies
echo "--> Installing extensions and PHP dependencies via Composer"
docker exec docker-compose_mediawiki_1 php composer.phar install
docker exec docker-compose_mediawiki_1 ln -s /var/www/html/extensions/PageForms /var/www/html/extensions/SemanticForms

# Enable SMW
docker exec docker-compose_mediawiki_1 bash -c "echo \"enableSemantics( 'localhost' );\" >> LocalSettings.php"

# Enable ParserFunctions
docker exec docker-compose_mediawiki_1 bash -c "echo \"wfLoadExtension( 'ParserFunctions' );\" >> LocalSettings.php"

# Download and enable Foreground skin
echo "--> Installing skins"
docker exec docker-compose_mediawiki_1 bash -c 'curl -sL https://github.com/jthingelstad/foreground/archive/fb62d6e03ebfe83cbe970013d2a6c16d3ef36c59.zip -o foreground.zip && unzip -qq foreground.zip -d skins && mv skins/foreground-fb62d6e03ebfe83cbe970013d2a6c16d3ef36c59 skins/foreground && rm foreground.zip'
docker exec docker-compose_mediawiki_1 bash -c "echo \"wfLoadSkin( 'foreground' );\" >> LocalSettings.php"
docker exec docker-compose_mediawiki_1 sed -i 's/$wgDefaultSkin = "vector";/$wgDefaultSkin = "foreground";/' LocalSettings.php

# Enable image uploads and InstantCommons
docker exec docker-compose_mediawiki_1 sed -i 's/$wgUseInstantCommons = false;/$wgUseInstantCommons = true;/' LocalSettings.php
docker exec docker-compose_mediawiki_1 sed -i 's/$wgEnableUploads = false;/$wgEnableUploads = true;/' LocalSettings.php

# Set site language (some links depend on CA localisation)
docker exec docker-compose_mediawiki_1 sed -i 's/$wgLanguageCode = "en";/$wgLanguageCode = "ca";/' LocalSettings.php

# Download and enable Variables
echo "--> Installing extensions"
docker exec docker-compose_mediawiki_1 bash -c 'curl -sL https://github.com/wikimedia/mediawiki-extensions-Variables/archive/REL1_27.zip -o extension.zip && unzip -qq extension.zip -d extensions && mv extensions/mediawiki-extensions-Variables-REL1_27 extensions/Variables && rm extension.zip'
docker exec docker-compose_mediawiki_1 bash -c "echo \"require_once( \\\"\\\$IP/extensions/Variables/Variables.php\\\" );\" >> LocalSettings.php"

# Download and enable Arrays
docker exec docker-compose_mediawiki_1 bash -c 'curl -sL https://github.com/wikimedia/mediawiki-extensions-Arrays/archive/REL1_27.zip -o extension.zip && unzip -qq extension.zip -d extensions && mv extensions/mediawiki-extensions-Arrays-REL1_27 extensions/Arrays && rm extension.zip'
docker exec docker-compose_mediawiki_1 bash -c "echo \"require_once( \\\"\\\$IP/extensions/Arrays/Arrays.php\\\" );\" >> LocalSettings.php"

# Download and enable External Data (probably not needed)
docker exec docker-compose_mediawiki_1 bash -c 'curl -sL https://github.com/wikimedia/mediawiki-extensions-ExternalData/archive/REL1_27.zip -o extension.zip && unzip -qq extension.zip -d extensions && mv extensions/mediawiki-extensions-ExternalData-REL1_27 extensions/ExternalData && rm extension.zip'
docker exec docker-compose_mediawiki_1 bash -c "echo \"wfLoadExtension( 'ExternalData' );\" >> LocalSettings.php"

# This is needed for svg thumbnail generation
docker exec docker-compose_mediawiki_1 bash -c "echo \"\\\$wgMaxShellMemory = 202400;\" >> LocalSettings.php"

# Run DB update script
docker exec docker-compose_mediawiki_1 php maintenance/update.php --quick

exit 0

